def insert_school(hash)
  query = "insert into public.schools(id, name, country, city, state, url, picture, created_at, updated_at) values(:id, :name, :country, :city, :state, :url, :picture, :created_at, :updated_at)"
  prepared_hash = {}
  hash.each do |k, v|
    prepared_hash[k.downcase] = v
  end
  prepared_hash = prepared_hash.merge("url" => hash['Website'])
  prepared_hash = prepared_hash.merge("picture" => hash['Picture'])
  prepared_hash.delete("website")
  #prepared_hash.delete("picture")
  prepared_hash['created_at'] = prepared_hash['updated_at'] = Time.now
  sql = nil
  if Rails.env == 'production'
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [query, prepared_hash.with_indifferent_access])
    puts sql
    ActiveRecord::Base.connection.execute(sql)
  else
    puts prepared_hash
    School.create prepared_hash
  end
end
def set_next_val(tablename, tablesequence)
  query = "SELECT setval('#{tablesequence}', (SELECT MAX(id) FROM #{tablename}))"
  puts query
  ActiveRecord::Base.connection.execute(query)
end
def insert_major(hash)
  query = "insert into public.majors(id, name, created_at, updated_at) values(:id, :name, :created_at, :updated_at)"
  prepared_hash = {}
  hash.each do |k, v|
    prepared_hash[k.downcase] = v
  end
  prepared_hash = prepared_hash.merge("name" => hash['Major name'])
  prepared_hash.delete("major name")
  prepared_hash['created_at'] = prepared_hash['updated_at'] = Time.now
  sql = nil
  if Rails.env == 'production'
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [query, prepared_hash.with_indifferent_access])
    puts sql
    ActiveRecord::Base.connection.execute(sql)
  else
    puts prepared_hash
    Major.create prepared_hash
  end
end
def insert_schools_majors(hash)
  query = "insert into public.schools_majors(id, school_id, major_id, created_at, updated_at) values(:id, :school_id, :major_id, :created_at, :updated_at)"
  prepared_hash = {}
  prepared_hash['school_id'] = hash['School id']
  prepared_hash['major_id'] = hash['Major id']
  prepared_hash['id'] = hash['id']

  prepared_hash['created_at'] = prepared_hash['updated_at'] = Time.now

  sql = nil
  if Rails.env == 'production'
    sql = ActiveRecord::Base.send(:sanitize_sql_array, [query, prepared_hash.with_indifferent_access])
    puts sql
    ActiveRecord::Base.connection.execute(sql)
  else
    puts prepared_hash
    SchoolsMajor.create prepared_hash
  end
end

require 'csv'
data = []
CSV.foreach('db/seeds/schools.csv', headers: true) do |row|
  hash = row.to_hash
  data << hash
  insert_school(hash)
end
CSV.foreach('db/seeds/majors.csv', headers: true) do |row|
  hash = row.to_hash
  data << hash
  insert_major(hash)
end
CSV.foreach('db/seeds/schools_majors.csv', headers: true) do |row|
  hash = row.to_hash
  data << hash
  insert_schools_majors(hash)
end

set_next_val("public.majors", 'majors_id_seq')
set_next_val("public.schools", 'schools_id_seq')
set_next_val("public.schools_majors", 'schools_majors_id_seq')
