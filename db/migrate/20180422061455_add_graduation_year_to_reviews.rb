class AddGraduationYearToReviews < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :graduation_year, :string
  end
end
