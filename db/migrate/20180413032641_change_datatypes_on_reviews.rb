class ChangeDatatypesOnReviews < ActiveRecord::Migration[5.0]
  def down
    change_column :reviews, :pro, :string
    change_column :reviews, :con, :string
  end

  def up
    change_column :reviews, :pro, :text
    change_column :reviews, :con, :text
  end
end
