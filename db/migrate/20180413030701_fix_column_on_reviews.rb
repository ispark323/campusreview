class FixColumnOnReviews < ActiveRecord::Migration[5.0]
  def change
    rename_column :reviews, :one_line_view, :oneline_review
  end
end
