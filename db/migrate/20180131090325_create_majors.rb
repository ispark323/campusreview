class CreateMajors < ActiveRecord::Migration[5.0]
  def change
    create_table :majors do |t|
      t.string :name
      t.timestamps
    end
    add_index :majors, :name, unique: true
  end
end
