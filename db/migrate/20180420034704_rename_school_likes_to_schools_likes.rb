class RenameSchoolLikesToSchoolsLikes < ActiveRecord::Migration[5.0]
  def change
    rename_table :school_likes, :schools_likes
  end
end
