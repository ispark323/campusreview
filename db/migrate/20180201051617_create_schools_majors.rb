class CreateSchoolsMajors < ActiveRecord::Migration[5.0]
  def change
    ActiveRecord::Base.logger = Logger.new STDOUT
    create_table :schools_majors do |t|
      t.references :school, foreign_key: true
      t.references :major, foreign_key: true

      t.timestamps
    end
  end
end
