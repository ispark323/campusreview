class RenameReviewLikesToReviewsLikes < ActiveRecord::Migration[5.0]
  def change
    rename_table :review_likes, :reviews_likes
  end
end
