class CreateLikesTables < ActiveRecord::Migration[5.0]
  def change
     create_table :school_likes do |t|
       t.belongs_to :user
       t.belongs_to :schools
       t.timestamps
     end

     create_table :review_likes do |t|
       t.belongs_to :user
       t.belongs_to :reviews
       t.timestamps
     end
  end
end
