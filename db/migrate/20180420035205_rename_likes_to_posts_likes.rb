class RenameLikesToPostsLikes < ActiveRecord::Migration[5.0]
  def change
    rename_table :likes, :posts_likes
  end
end
