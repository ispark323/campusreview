class CreateSchools < ActiveRecord::Migration[5.0]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :country
      t.string :state
      t.string :city
      t.string :address
      t.string :postcode
      t.string :url
      t.string :picture
      t.string :logo
      t.timestamps
    end
  end
end
