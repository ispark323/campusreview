class AddColumnOnSchools < ActiveRecord::Migration[5.0]
  def change
    add_column :schools, :oneline_review, :string
  end
end
