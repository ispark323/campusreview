class FixColumnOnReviewsLikesAndSchoolsLikes < ActiveRecord::Migration[5.0]
  def change
    rename_column :reviews_likes, :reviews_id, :review_id
    rename_column :schools_likes, :schools_id, :school_id
  end
end
