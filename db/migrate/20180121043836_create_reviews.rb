class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.belongs_to :schools_majors
      t.belongs_to :user
      t.string :pro
      t.string :con
      t.string :advice
      t.string :one_line_view
      t.integer :rating_overall
      t.integer :rating_friendly
      t.integer :rating_teaching
      t.integer :rating_location
      t.integer :rating_facilities
      t.integer :rating_opportunity
      t.integer :rating_value
      t.integer :recommended
      t.integer :user_type
      t.boolean :anonymous

      t.timestamps
    end
  end
end
