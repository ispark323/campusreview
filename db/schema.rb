# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180427052548) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_comments_on_post_id", using: :btree
    t.index ["user_id"], name: "index_comments_on_user_id", using: :btree
  end

  create_table "follows", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["followed_id"], name: "index_follows_on_followed_id", using: :btree
    t.index ["follower_id", "followed_id"], name: "index_follows_on_follower_id_and_followed_id", unique: true, using: :btree
    t.index ["follower_id"], name: "index_follows_on_follower_id", using: :btree
  end

  create_table "majors", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_majors_on_name", unique: true, using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.text     "content"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "image"
    t.index ["user_id"], name: "index_posts_on_user_id", using: :btree
  end

  create_table "posts_likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_posts_likes_on_post_id", using: :btree
    t.index ["user_id"], name: "index_posts_likes_on_user_id", using: :btree
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "schools_majors_id"
    t.integer  "user_id"
    t.text     "pro"
    t.text     "con"
    t.string   "advice"
    t.string   "oneline_review"
    t.integer  "rating_overall"
    t.integer  "rating_friendly"
    t.integer  "rating_teaching"
    t.integer  "rating_location"
    t.integer  "rating_facilities"
    t.integer  "rating_opportunity"
    t.integer  "rating_value"
    t.integer  "recommended"
    t.integer  "user_type"
    t.boolean  "anonymous"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "graduation_year"
    t.index ["schools_majors_id"], name: "index_reviews_on_schools_majors_id", using: :btree
    t.index ["user_id"], name: "index_reviews_on_user_id", using: :btree
  end

  create_table "reviews_likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "review_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["review_id"], name: "index_reviews_likes_on_review_id", using: :btree
    t.index ["user_id"], name: "index_reviews_likes_on_user_id", using: :btree
  end

  create_table "schools", force: :cascade do |t|
    t.string   "name"
    t.string   "country"
    t.string   "state"
    t.string   "city"
    t.string   "address"
    t.string   "postcode"
    t.string   "url"
    t.string   "picture"
    t.string   "logo"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "oneline_review"
  end

  create_table "schools_likes", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["school_id"], name: "index_schools_likes_on_school_id", using: :btree
    t.index ["user_id"], name: "index_schools_likes_on_user_id", using: :btree
  end

  create_table "schools_majors", force: :cascade do |t|
    t.integer  "school_id"
    t.integer  "major_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["major_id"], name: "index_schools_majors_on_major_id", using: :btree
    t.index ["school_id"], name: "index_schools_majors_on_school_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "name"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "avatar"
    t.boolean  "admin",                  default: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "schools_majors", "majors"
  add_foreign_key "schools_majors", "schools"
end
