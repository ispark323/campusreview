require 'test_helper'

class ReviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @review = reviews(:one)
  end

  test "should get index" do
    get reviews_url
    assert_response :success
  end

  test "should get new" do
    get new_review_url
    assert_response :success
  end

  test "should create review" do
    assert_difference('Review.count') do
      post reviews_url, params: { review: { advice: @review.advice, anonymous: @review.anonymous, con: @review.con, oneline_review: @review.oneline_review, pro: @review.pro, rating_facilities: @review.rating_facilities, rating_friendly: @review.rating_friendly, rating_location: @review.rating_location, rating_opportunity: @review.rating_opportunity, rating_overall: @review.rating_overall, rating_teaching: @review.rating_teaching, rating_value: @review.rating_value, recommended: @review.recommended, schools_majors_id: @review.schools_majors_id, user_id: @review.user_id, user_type: @review.user_type } }
    end

    assert_redirected_to review_url(Review.last)
  end

  test "should show review" do
    get review_url(@review)
    assert_response :success
  end

  test "should get edit" do
    get edit_review_url(@review)
    assert_response :success
  end

  test "should update review" do
    patch review_url(@review), params: { review: { advice: @review.advice, anonymous: @review.anonymous, con: @review.con, oneline_review: @review.oneline_review, pro: @review.pro, rating_facilities: @review.rating_facilities, rating_friendly: @review.rating_friendly, rating_location: @review.rating_location, rating_opportunity: @review.rating_opportunity, rating_overall: @review.rating_overall, rating_teaching: @review.rating_teaching, rating_value: @review.rating_value, recommended: @review.recommended, schools_majors_id: @review.schools_majors_id, user_id: @review.user_id, user_type: @review.user_type } }
    assert_redirected_to review_url(@review)
  end

  test "should destroy review" do
    assert_difference('Review.count', -1) do
      delete review_url(@review)
    end

    assert_redirected_to reviews_url
  end
end
