require 'test_helper'

class SchoolsMajorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @schools_major = schools_majors(:one)
  end

  test "should get index" do
    get schools_majors_url
    assert_response :success
  end

  test "should get new" do
    get new_schools_major_url
    assert_response :success
  end

  test "should create schools_major" do
    assert_difference('SchoolsMajor.count') do
      post schools_majors_url, params: { schools_major: { major_id: @schools_major.major_id, school_id: @schools_major.school_id } }
    end

    assert_redirected_to schools_major_url(SchoolsMajor.last)
  end

  test "should show schools_major" do
    get schools_major_url(@schools_major)
    assert_response :success
  end

  test "should get edit" do
    get edit_schools_major_url(@schools_major)
    assert_response :success
  end

  test "should update schools_major" do
    patch schools_major_url(@schools_major), params: { schools_major: { major_id: @schools_major.major_id, school_id: @schools_major.school_id } }
    assert_redirected_to schools_major_url(@schools_major)
  end

  test "should destroy schools_major" do
    assert_difference('SchoolsMajor.count', -1) do
      delete schools_major_url(@schools_major)
    end

    assert_redirected_to schools_majors_url
  end
end
