#!/bin/bash

bundle check

bundle exec rake db:create
bundle exec rake db:migrate
bundle exec rake db:seed
bundle exec rails s -b 0.0.0.0
