class UsersController < ApplicationController
  before_action :authorizeme
  def index
    @users = User.all
  end

  def my_page
  end

	def delete
		Review.destroy params[:rev]
		redirect_to user_my_reviews_delete_path
	end

  def my_reviews
    @reviews = current_user.reviews
  end

  def collected_reviews
    @liked_reviews = current_user.reviews_likes
  end

  def my_schools
    # liked schools
    @liked_schools = current_user.schools_likes.map {|schools_like| schools_like.school.name}
  end
end
