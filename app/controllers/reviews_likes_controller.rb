class ReviewsLikesController < ApplicationController
	before_action :authenticate_user!
	
	def review_like_toggle
		reviews_like = ReviewsLike.find_by(user_id: current_user.id, review_id: params[:review_id])

		if reviews_like.present?
			reviews_like.destroy
		else
			ReviewsLike.create(user_id: current_user.id, review_id: params[:review_id])
		end 
	end
end
