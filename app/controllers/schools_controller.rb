class SchoolsController < ApplicationController
  before_action :set_school, only: [:show, :edit, :update, :destroy, :reviews]
  # before_action :authorizeadmin, only: [:index]
  autocomplete :school, :name, full: true

  def school_reviews
    @school = School.all.limit(1).order("RANDOM()")[0]
    # @reviews = Review.joins(:reviews_likes).limit(10).order("RANDOM()")
    # review with likes
    @reviews = Review.all.limit(10).order("RANDOM()")
  end

  # GET /schools
  # GET /schools.json
  # def index
  #   @schools = School.all
  # end

#SELECT  schools.id, schools.name FROM "schools" WHERE (LOWER(schools.name) ILIKE '%au%') ORDER BY LOWER(schools.name) ASC LIMIT $1
  def search_school
    if params[:id]
      set_school
      redirect_to @school
    end
  end

  def my_schools
    @school = School.all
  end

  def show
    reviews
  end
  # GET /schools/reviews/1
  # GET /schools/reviews/1.json
  def reviews
    @reviews = @school.schools_majors.collect{|r| r.reviews}.flatten
    data = %W(rating_overall rating_teaching rating_location rating_facilities rating_opportunity rating_value rating_friendly).collect{|key|
    x = @reviews.collect{|t| t.send(key.to_sym)}.select{|t| t.to_i > 0 && t.to_i < 6}

    avg = x.sum / x.length rescue nil # case for 0 reviews
    [key, avg]
    }
    @data = Hash[data]
    ranks = Struct.new(:rating_overall, :rating_teaching, :rating_location, :rating_facilities, :rating_opportunity, :rating_value, :rating_friendly)
    @review = ranks.new(data[0][1], data[1][1], data[2][1], data[3][1], data[4][1], data[5][1], data[6][1])
    @recommend_percentage = @reviews.select{|t| t.recommended.to_i > 0}.length.to_f / @reviews.length * 100.to_f rescue "N/A"
  end

  # GET /schools/new
  def new
    @school = School.new
  end

  # GET /schools/1/edit
  def edit
  end

  # POST /schools
  # POST /schools.json
  def create
    @school = School.new(school_params)

    respond_to do |format|
      if @school.save
        format.html { redirect_to @school, notice: 'School was successfully created.' }
        format.json { render :show, status: :created, location: @school }
      else
        format.html { render :new }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schools/1
  # PATCH/PUT /schools/1.json
  def update
    respond_to do |format|
      if @school.update(school_params)
        format.html { redirect_to @school, notice: 'School was successfully updated.' }
        format.json { render :show, status: :ok, location: @school }
      else
        format.html { render :edit }
        format.json { render json: @school.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schools/1
  # DELETE /schools/1.json
  def destroy
    @school.destroy
    respond_to do |format|
      format.html { redirect_to schools_url, notice: 'School was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_school
      @school = School.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def school_params
      params.require(:school).permit(:name, :country, :state, :city, :address, :postcode, :url, :picture, :logo)
    end
end
