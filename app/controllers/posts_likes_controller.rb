class PostsLikesController < ApplicationController
  before_action :authenticate_user!

  def like_toggle
    post_like = PostsLike.find_by(user_id: current_user.id,
                                  post_id: params[:post_id])

    if post_like.nil?
      Like.create(user_id: current_user.id,
                  post_id: params[:post_id])
    else
      like.destroy
    end

    redirect_to :back
  end
end
