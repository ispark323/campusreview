class SchoolsLikesController < ApplicationController
	before_action :authenticate_user!
	
	def school_like_toggle
		schools_like = SchoolsLike.find_by(user_id: current_user.id, school_id: params[:school_id])

		if schools_like.present?
			schools_like.destroy
		else
			SchoolsLike.create(user_id: current_user.id, school_id: params[:school_id])
		end 
	end
end
