class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
      devise_parameter_sanitizer.permit(:account_update, keys: [:name])
    end

    def authorizeme
      redirect_to new_user_session_path, alert: "not authenticated." if current_user.nil?
    end

    def authorizeadmin
      unless( current_user && current_user.email == "spark@nxtpath.org" )
        redirect_to new_user_session_path, notice: "Log in with admin account"
      end
    end
end