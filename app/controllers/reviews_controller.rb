class ReviewsController < ApplicationController
  before_action :set_review, only: [:show, :edit, :update, :destroy]
  before_action :authorizeme, only: [:edit, :update, :create, :new]

  # GET /reviews/1
  # GET /reviews/1.json
  def show
  end

  # GET /reviews/new
  def new
    @review = Review.new
  end

  # GET /reviews/1/edit
  def edit
  end

  # POST /reviews
  # POST /reviews.json
  def create
    @review = Review.new(review_params)
    set_schools_major
    respond_to do |format|
      if @review.save
        format.html { redirect_to @review, notice: 'Review was successfully created.' }
        format.json { render :show, status: :created, location: @review }
      else
        format.html { render :new }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
    respond_to do |format|
      if @review.update(review_params)
        format.html { redirect_to @review, notice: 'Review was successfully updated.' }
        format.json { render :show, status: :ok, location: @review }
      else
        format.html { render :edit }
        format.json { render json: @review.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Review was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def major_list_options
    return render json: SchoolsMajor.list_majors_of_school(params[:school_id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_review
      @review = Review.find_by(id: params[:id])
    end

    def set_schools_major
      @review.schools_majors_id = Review.schools_major(params[:school], params[:major]).id
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(
        :pro, :con, :advice, :oneline_review, :recommended,
        :user_type
      ).merge(
        graduation_year:    params[:graduation_year]["date(1i)"],
        rating_overall:     params[:rating_overall_count],
        rating_teaching:    params[:rating_teaching_count],
        rating_location:    params[:rating_location_count],
        rating_facilities:  params[:rating_facilities_count],
        rating_friendly:    params[:rating_friendly_count],
        rating_opportunity: params[:rating_opportunity_count],
        rating_value:       params[:rating_value_count],
        user_id:            current_user.id
      )
    end
end
