class SchoolsMajorsController < ApplicationController
  before_action :set_schools_major, only: [:show, :edit, :update, :destroy]
  before_action :check_user_admin
  
  # GET /schools_majors
  # GET /schools_majors.json
  # def index
  #   @schools_majors = SchoolsMajor.all
  # end

  def check_user_admin
    if !current_user.admin
      redirect_to root_path
    end
    # ruby way
    # redirect_to root_path unless current_user.admin
  end

  # GET /schools_majors/1
  # GET /schools_majors/1.json
  def show
  end

  # GET /schools_majors/new
  def new
    @schools_major = SchoolsMajor.new
  end

  # GET /schools_majors/1/edit
  def edit
  end

  # POST /schools_majors
  # POST /schools_majors.json
  def create
    @schools_major = SchoolsMajor.new(schools_major_params)

    respond_to do |format|
      if @schools_major.save
        format.html { redirect_to @schools_major, notice: 'Schools major was successfully created.' }
        format.json { render :show, status: :created, location: @schools_major }
      else
        format.html { render :new }
        format.json { render json: @schools_major.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schools_majors/1
  # PATCH/PUT /schools_majors/1.json
  def update
    respond_to do |format|
      if @schools_major.update(schools_major_params)
        format.html { redirect_to @schools_major, notice: 'Schools major was successfully updated.' }
        format.json { render :show, status: :ok, location: @schools_major }
      else
        format.html { render :edit }
        format.json { render json: @schools_major.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schools_majors/1
  # DELETE /schools_majors/1.json
  def destroy
    @schools_major.destroy
    respond_to do |format|
      format.html { redirect_to schools_majors_url, notice: 'Schools major was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schools_major
      @schools_major = SchoolsMajor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schools_major_params
      params.require(:schools_major).permit(:school_id, :major_id)
    end
end
