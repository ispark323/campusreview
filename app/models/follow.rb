class Follow < ApplicationRecord
  # Relation : follower
  # Moden : User
  belongs_to :follower, class_name: "User"
  
  # Relation : followed
  # Moden : User
  belongs_to :followed, class_name: "User"
end
