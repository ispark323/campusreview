require 'csv'
class Review < ApplicationRecord
  belongs_to :schools_major, class_name: SchoolsMajor, foreign_key: :schools_majors_id
  belongs_to :user

  has_many :reviews_likes

  # delegate :school, :major, to: :schools_major

  def name_or_anon
    self.anonymous? ? "Anonymous" : user.try(:name).to_s
  end

  def self.schools_major(school_id, major_id)
    SchoolsMajor.find_by(school_id: school_id, major_id: major_id)
  end

  def determine_user_type
    user_types = {
      '1': "Current Student",
      '2': "#{self.graduation_year} Graduate",
      '3': "Staff"
    }
    user_types[self.user_type.to_s.to_sym]
  end

  def has_graduation_year
		self.persisted? && self.graduation_year?
	end
 
  def self.import_review
		data = []
		CSV.foreach('db/seeds/reviews.csv', headers: true) do |row|
		  hash = row.to_hash
		  data << hash
		end

#		review_id = Review.last.id + 1
		#review = Review.find_by_id data_row[:review_id]
		data.each do |data_row|
      data_row.symbolize_keys!
		
			#review = Review.find_by_id data_row[:id]
			#if review
				#review.delete
			#end

    	puts "going to create"
			review = Review.new
#			review.id = review_id#data_row[:id].to_i
			review.schools_majors_id = data_row[:"schools_majors_id"].to_i
			review.user_id = data_row[:"user_id"].to_i
			review.pro = data_row[:pro]
			review.con = data_row[:con]
			review.advice = data_row[:advice]
			review.oneline_review = data_row[:"oneline_review"]
			review.rating_overall = data_row[:"rating_overall"].to_i
			review.rating_friendly = data_row[:"rating_friendly"].to_i
			review.rating_teaching = data_row[:"rating_teaching"].to_i
			review.rating_location = data_row[:"rating_location"].to_i
			review.rating_facilities = data_row[:"rating_facilities"].to_i
			review.rating_opportunity = data_row[:"rating_opportunity"].to_i
			review.rating_value = data_row[:"rating_value"].to_i
			review.recommended = data_row[:recommended].to_i
			review.user_type = data_row[:"user_type"]
			review.graduation_year = data_row[:"graduation_year"]
			review.save
#			review_id = review_id + 1
    end
  end
end
