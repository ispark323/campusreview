require 'csv'
class School < ApplicationRecord
  #attr_accessible :picture

	has_many :schools_majors
	has_many :schools_likes
  has_many :majors, through: :schools_majors
  mount_uploader :picture, PictureUploader
  mount_uploader :logo, LogoUploader

  def one_line_comment
    "this is school one line comment."
  end

  def self.mapped_collection
    all.order(name: :asc).collect {|school| [ school.name, school.id ] }
  end

	def self.update_school_from_csv
		CSV.parse(File.read('db/seeds/schools.csv'), :headers => true).each do |row|
			school = School.find_by(id: row['id'], name: row['Name'], country: row['Country'])

			if school
				school.update_columns(oneline_review: row['oneline_review'])
			else
				school_params = {
					id: row['id'], name: row['Name'], country: row['Country'],
					state: row['State'], city: row['City'], url: row['Website'],
					oneline_review: row['oneline_review']
				}
				School.create(school_params)
			end
		end
	end

  def self.import_school_picture_path
    data = []
		CSV.foreach('db/seeds/schools.csv', headers: true) do |row|
			hash = row.to_hash
			data << hash
		end
		current_path = File.expand_path(File.dirname(__FILE__))
		images_path = File.expand_path("../assets/images", current_path)

		data.each do |data_row|
			data_row.symbolize_keys!
			school = nil
			school = School.find data_row[:id]

			unless data_row[:Picture].nil?
				path = images_path+"/"+data_row[:Picture]

				if school
					if File.exist?(path)
						file = File.open(path)
						school.picture=file
						school.save
					end
				end
			end
		end
  end
end
