class Major < ApplicationRecord
  has_many :schools_majors
  has_many :schools, through: :schools_majors
  validates_presence_of :name

  def self.mapped_collection
    all.order(name: :asc).collect {|major| [ major.name, major.id ] }
  end
end
