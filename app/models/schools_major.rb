class SchoolsMajor < ApplicationRecord
  belongs_to :school
  belongs_to :major
  has_many :reviews, foreign_key: :schools_majors_id

  def display_name
    school_name = school.name
    major_name = major.name
    "#{school_name} #{major_name}"
  end

  def self.list_majors_of_school(school_id)
    data = {}
    schools_majors = self.includes(:school, :major).order('majors.name').where(school_id: school_id)

    schools_majors.map{|s_m| data["#{s_m.major.name}"] = "#{s_m.major.id}"}
    data.to_json
  end

  def rating_overall_averate
    all_review_ratings = self.reviews.pluck(:rating_overall)
    format("%.1f",(all_review_ratings.inject(0){|sum,x| sum + x }.to_f/ all_review_ratings.size))
  end
end
