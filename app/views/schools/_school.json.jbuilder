json.extract! school, :id, :name, :country, :state, :city, :address, :postcode, :url, :picture, :logo, :created_at, :updated_at
json.url school_url(school, format: :json)
