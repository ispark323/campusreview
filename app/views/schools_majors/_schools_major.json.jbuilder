json.extract! schools_major, :id, :school_id, :major_id, :created_at, :updated_at
json.url schools_major_url(schools_major, format: :json)
