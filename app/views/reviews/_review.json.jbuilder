json.extract! review, :id, :schools_majors_id, :user_id, :pro, :con, :advice, :oneline_review, :rating_overall, :rating_friendly, :rating_teaching, :rating_location, :rating_facilities, :rating_opportunity, :rating_value, :recommended, :user_type, :anonymous, :created_at, :updated_at
json.url review_url(review, format: :json)
