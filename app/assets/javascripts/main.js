/*
	Editorial by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {
  $('#graduation_year_section').hide();

	if($('#review_user_type').val() == '2'){
		$('#graduation_year_section').show();
	} else {
		$('#graduation_year_section').hide();
		$('#graduation_year_date_1i').val("")
	}

	$( document ).delegate( '#graduate_radio', "click", function() {
		$('#graduation_year_section').show();
	});
	$( document ).delegate( '#staff_radio', "click", function() {
		$('#graduation_year_section').hide();
		$('#graduation_year_date_1i').val("")
	});
	$( document ).delegate( '#current_student_radio', "click", function() {
		$('#graduation_year_section').hide();
		$('#graduation_year_date_1i').val("")
	});

	var school_liked = $('#user_school_like').val()
	if(school_liked === 'true'){
		$('#school_like_toggle').addClass('pink')
	}

	$('#school_like_toggle').on('click', function(){
		if($(this).hasClass('pink')){	// already like
			$(this).removeClass('pink')
		} else {						// currently not liked
			$(this).addClass('pink')
			console.log('toggling part');
		}

		var schoolId = window.location.pathname.split('/')[2]
		console.log(schoolId);
		$.ajax({
			type:"POST",
			url:"/school_like_toggle",
			dataType:"json",
			data: {school_id: schoolId},
			success:function(result){
				console.log('school liked!');
			}
		});
	});


	var review_liked = $('#user_review_like').val()
	if(review_liked === 'true'){
		$('#review_like_toggle').addClass('pink')
	}

	$('#review_like_toggle').on('click', function(){
		if($(this).hasClass('pink')){	// already like
			$(this).removeClass('pink')
		} else {						// currently not liked
			$(this).addClass('pink')
			console.log('toggling part');
		}

		var reviewId = window.location.pathname.split('/')[2]
		console.log(reviewId);
		$.ajax({
			type:"POST",
			url:"/review_like_toggle",
			dataType:"json",
			data: {review_id: reviewId},
			success:function(result){
				console.log('liked!');
			}
		});
	});

	// dynamic major dropdown selector
	$("#school_").on('change', function(){
		// detect change in school value - grab schoolId
		var schoolId = $(this).val();

		// remove all major selections for population
		var majorDropdown = $('#major_select');
		 $('#major_select option').remove();

		 // store majors in dynamicOptions based on school value
		 var dynamicOptions = null

		 $.ajax({
			  type:"GET",
			  url:"/reviews/major_list_options",
			  dataType:"json",
			  data: {school_id: schoolId},
			  success:function(result){

					// ajax call complete
					dynamicOptions = result
					if (dynamicOptions != null){
						// populate options with majors
						$.each(dynamicOptions, function(key, value) {
							$('#options_major_select').append(
								$('<option>', { value: value, text: key}));
						});
						// prepend select nil
						$('#options_major_select').prepend(
							"<option value='' selected='selected'></option>"
						);
					} else {
						console.log('Cannot find Majors for School')
					}
			  }
			});
	});

	$('.myselect').select2();

	// word count indicator
	var textAreas = [
		'150,#review_advice,#advice_text',
		'500,#review_con,#con_text',
		'500,#review_pro,#pro_text',
		'70,#review_oneline_review,#oneline_text'
	];

	var indicateWordCount = function(maxLength, inputTextArea, indicator){
		$(document).on('keydown', inputTextArea, function() {
				var wordLength = $(inputTextArea).val().length;
				$(indicator).text(
					wordLength + ' / ' + 'max '+ maxLength +' chars'
				);
		});
	}

	for(var i = 0 ; i < textAreas.length ; i++){
		elements = textAreas[i].split(',')
		indicateWordCount(elements[0], elements[1], elements[2])
	}

	// initialize star ratings on page load
	var allRatings = [
		'#rating_overall',
		'#rating_teaching',
		'#rating_location',
		'#rating_facilities',
		'#rating_friendly',
		'#rating_opportunity',
		'#rating_value'
	]

	$(document).ready(function(){

		function choice(selector, val, max) {
			var b = jQuery(selector);
			for (var i = 0; i < max; i++) {
				var bb = b.clone();
				bb.insertAfter(b);
				bb.click(function() {
					jQuery(this).prevAll().andSelf().removeClass("off").addClass("on").css("color", "#ffd432");
					jQuery(this).nextAll().removeClass("on").addClass("off").css("color", "#aaa");
				});
			}
			jQuery(selector + ":eq(0)").remove();
			jQuery(selector + ":eq(" + (val - 1) + ")").click();
		}

		// setting stars on page load
		for(var i = 0 ; i < allRatings.length ; i++){
			var rating = $(allRatings[i] +'_count').val()
			choice(allRatings[i] + ' .star', rating, 5);
		}

		$('.star').on('click', function(){
			var rating_selected = $(this).parent().attr('id');
			var rating_value = $(this).parent().find('.on').length
			$('#' + rating_selected + '_count').val(rating_value);
			console.log($('#' + rating_selected + '_count').val());
		})
	})

	skel.breakpoints({
		xlarge: '(max-width: 1680px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)',
		'xlarge-to-max': '(min-width: 1681px)',
		'small-to-xlarge': '(min-width: 481px) and (max-width: 1680px)'
	});

	$(function() {

		var	$window = $(window),
			$head = $('head'),
			$body = $('body');

		// Disable animations/transitions ...

			// ... until the page has loaded.
				$body.addClass('is-loading');

				$window.on('load', function() {
					setTimeout(function() {
						$body.removeClass('is-loading');
					}, 100);
				});

			// ... when resizing.
				var resizeTimeout;

				$window.on('resize', function() {

					// Mark as resizing.
						$body.addClass('is-resizing');

					// Unmark after delay.
						clearTimeout(resizeTimeout);

						resizeTimeout = setTimeout(function() {
							$body.removeClass('is-resizing');
						}, 100);

				});

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		// Fixes.

			// Object fit images.
				if (!skel.canUse('object-fit')
				||	skel.vars.browser == 'safari')
					$('.image.object').each(function() {

						var $this = $(this),
							$img = $this.children('img');

						// Hide original image.
							$img.css('opacity', '0');

						// Set background.
							$this
								.css('background-image', 'url("' + $img.attr('src') + '")')
								.css('background-size', $img.css('object-fit') ? $img.css('object-fit') : 'cover')
								.css('background-position', $img.css('object-position') ? $img.css('object-position') : 'center');

					});

		// Sidebar.
			var $sidebar = $('#sidebar'),
				$sidebar_inner = $sidebar.children('.inner');

			// Inactive by default on <= large.
				skel
					.on('+large', function() {
						$sidebar.addClass('inactive');
					})
					.on('-large !large', function() {
						$sidebar.removeClass('inactive');
					});

			// Hack: Workaround for Chrome/Android scrollbar position bug.
				if (skel.vars.os == 'android'
				&&	skel.vars.browser == 'chrome')
					$('<style>#sidebar .inner::-webkit-scrollbar { display: none; }</style>')
						.appendTo($head);

			// Toggle.
				if (skel.vars.IEVersion > 9) {

					$('<a href="#sidebar" class="toggle">Toggle</a>')
						.appendTo($sidebar)
						.on('click', function(event) {

							// Prevent default.
								event.preventDefault();
								event.stopPropagation();

							// Toggle.
								$sidebar.toggleClass('inactive');

						});

				}

			// Events.

				// Link clicks.
					$sidebar.on('click', 'a', function(event) {

						// >large? Bail.
							if (!skel.breakpoint('large').active)
								return;

						// Vars.
							var $a = $(this),
								href = $a.attr('href'),
								target = $a.attr('target');

						// Prevent default.
							event.preventDefault();
							event.stopPropagation();

						// Check URL.
							if (!href || href == '#' || href == '')
								return;

						// Hide sidebar.
							$sidebar.addClass('inactive');

						// Redirect to href.
							setTimeout(function() {

								if (target == '_blank')
									window.open(href);
								else
									window.location.href = href;

							}, 500);

					});

				// Prevent certain events inside the panel from bubbling.
					$sidebar.on('click touchend touchstart touchmove', function(event) {

						// >large? Bail.
							if (!skel.breakpoint('large').active)
								return;

						// Prevent propagation.
							event.stopPropagation();

					});

				// Hide panel on body click/tap.
					$body.on('click touchend', function(event) {

						// >large? Bail.
							if (!skel.breakpoint('large').active)
								return;

						// Deactivate.
							$sidebar.addClass('inactive');

					});

			// Scroll lock.
			// Note: If you do anything to change the height of the sidebar's content, be sure to
			// trigger 'resize.sidebar-lock' on $window so stuff doesn't get out of sync.

				$window.on('load.sidebar-lock', function() {

					var sh, wh, st;

					// Reset scroll position to 0 if it's 1.
						if ($window.scrollTop() == 1)
							$window.scrollTop(0);

					$window
						.on('scroll.sidebar-lock', function() {

							var x, y;

							// IE<10? Bail.
								if (skel.vars.IEVersion < 10)
									return;

							// <=large? Bail.
								if (skel.breakpoint('large').active) {

									$sidebar_inner
										.data('locked', 0)
										.css('position', '')
										.css('top', '');

									return;

								}

							// Calculate positions.
								x = Math.max(sh - wh, 0);
								y = Math.max(0, $window.scrollTop() - x);

							// Lock/unlock.
								if ($sidebar_inner.data('locked') == 1) {

									if (y <= 0)
										$sidebar_inner
											.data('locked', 0)
											.css('position', '')
											.css('top', '');
									else
										$sidebar_inner
											.css('top', -1 * x);

								}
								else {

									if (y > 0)
										$sidebar_inner
											.data('locked', 1)
											.css('position', 'fixed')
											.css('top', -1 * x);

								}

						})
						.on('resize.sidebar-lock', function() {

							// Calculate heights.
								wh = $window.height();
								sh = $sidebar_inner.outerHeight() + 30;

							// Trigger scroll.
								$window.trigger('scroll.sidebar-lock');

						})
						.trigger('resize.sidebar-lock');

					});

		// Menu.
			var $menu = $('#menu'),
				$menu_openers = $menu.children('ul').find('.opener');

			// Openers.
				$menu_openers.each(function() {

					var $this = $(this);

					$this.on('click', function(event) {

						// Prevent default.
							event.preventDefault();

						// Toggle.
							$menu_openers.not($this).removeClass('active');
							$this.toggleClass('active');

						// Trigger resize (sidebar lock).
							$window.triggerHandler('resize.sidebar-lock');

					});

				});

	});

})(jQuery);
