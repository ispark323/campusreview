# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
Rails.application.config.assets.paths << Rails.root.join("app", "assets", "fonts")

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w(style.css ie9.css ie8.css html5shiv.js font-awesome.min util skel.min jquery.min main ie/respond.min respond.min)
Rails.application.config.assets.precompile << %r{fonts/[\w-]+\.(?:eot|svg|ttf|woff2?)$}

%w(eot svg ttf woff woff2).each do |ext|
  Rails.application.config.assets.precompile << "fontawesome-webfont.#{ext}"
end
