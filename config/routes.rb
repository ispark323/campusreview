Rails.application.routes.draw do
  get 'majors/autocomplete_major_name'
  get 'majors/search_major'
  get 'users/my_page'
  get 'users/my_reviews'
  get 'reviews/new'
  post 'reviews/new'
  get 'users/collected_reviews'
  get 'users/my_schools'

  resources :schools_majors, except: [:index]
  get 'majors/update_major_list', as: 'update_major_list'

  resources :majors
  devise_for :users

  root 'schools#school_reviews'

  get 'schools/autocomplete_school_name'
  get 'schools/search_school'

  # get 'reviews/write_review', as: 'write_a_review'
  resources :reviews, except: [:index] do
    collection do
      get 'major_list_options'
    end
  end

  resources :schools, except: [:index] do
    member do
      get 'reviews'
    end
  end

  get 'school_reviews', to: 'schools#school_reviews'
  get 'my_schools', to: 'schools#my_schools'

  resources :posts, except: [:show] do
    post "/like", to: "likes#like_toggle"
    resources :comments, only: [:create, :destroy]
  end
  resources :follows, only: [:create, :destroy]

  post 'review_like_toggle', to: 'reviews_likes#review_like_toggle'
  post 'school_like_toggle', to: 'schools_likes#school_like_toggle'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
